package br.com.syonet.exceptions;


public class Conta {
	
	public String cpfTitular;
	public String nome;
	public String banco;
	public Integer agencia;
	public Integer numConta;
	public Double saldo;
	
	
	public Conta(String cpfTitular, String nome, String banco,
			Integer agencia, Integer numConta, Double saldo) {
		super();
		this.cpfTitular = cpfTitular;
		this.nome = nome;
		this.banco = banco;;
		this.agencia = agencia;
		this.numConta = numConta;
		this.saldo = saldo;
	}
	
	public String getCpfTitular() {
		return cpfTitular;
	}
	public void setCpfTitular(String cpfTitular) {
		this.cpfTitular = cpfTitular;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome() {
		this.nome = nome;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public Integer getAgencia() {
		return agencia;
	}

	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}

	public Integer getNumConta() {
		return numConta;
	}

	public void setNumConta(Integer numConta) {
		this.numConta = numConta;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public String toString() {
		return "CPF: " + getCpfTitular() 
					   + " Nome: " 
					   + getNome()
					   + " Banco: " 
					   + getBanco()
					   + " Ag�ncia: "
					   + getAgencia()
					   + " N� Conta: "
					   + getNumConta()
					   + " Saldo: R$"
					   + getSaldo();
		}
	
	

}
