package br.com.syonet.exceptions;

public class ContaNaoEncontradaException extends Exception {

	public String mensagem;
	
	public ContaNaoEncontradaException(String mensagem) {
		super();
		this.mensagem = mensagem;
		
	}
	
	public String getMessage() {
		return String.format("N�o � conta cadastrada com esse CPF!", getStackTrace());
		
	}
		
		public String getMensagem() {
			return mensagem;
		}
		
		public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
		
	}
		public String toString() {
			return "CPF n�o encontrado" 
					+ getMensagem();
		}
}
