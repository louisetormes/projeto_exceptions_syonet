package br.com.syonet.exceptions;

public class SaldoNegativadoException extends Exception {
	public String mensagem;
	
	public SaldoNegativadoException(String mensagem) {
		super();
		this.mensagem = mensagem;
	}

}
