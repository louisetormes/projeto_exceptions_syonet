package br.com.syonet.exceptions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;



public class Main {

	public static List<Conta> contas;
	
	public static void main(String[] args) throws ContaNaoEncontradaException, SaldoNegativadoException {
		contas = Arrays.asList(
				new Conta("030-388-480-00", "Louise Saldanha Tormes", "Nubank", 0001, 123456, 50000.00),
				new Conta("027-644-123-11", "Maria da Silva", "Sicredi", 1234, 654321, 10000.00),
				new Conta("027-600-270-90", "Lucas Reis", "Itau", 4321, 555555, 200.00));
		contas.forEach(System.out::println);
		
		
		

		try {
			
			String cpf = buscarCPF("");
			
			if(cpf != null) {
				System.out.println(cpf);
				
			} 
		}
		catch (ContaNaoEncontradaException e) {
		String cpf = buscarCPF("");
			if(cpf == null) {
				throw new ContaNaoEncontradaException("Conta n�o Encontrada!");
		
		
			}	
		}
		
		
		try {
			Double saldo = consultaSaldo(contas);
			
			if(saldo != 0) {
				System.out.println(saldo);
			}
		}
		catch (SaldoNegativadoException s) {
			Double saldo = consultaSaldo(contas);
			
			if(saldo <= 0) {
				throw new SaldoNegativadoException("SaldoNegativo!");
			}
		}
	
		
	
		try {
			Double saque = saqueValor("Nubank", 1, 123456, 50.00);
			if(saque != null) {
				System.out.println(saque);
			}
		}
		catch (SaldoInsuficienteException a) {
			}
		}
		
	
	
	public static String buscarCPF(String cpfTitular) throws ContaNaoEncontradaException {
		contas.stream()
		.filter(c -> c.getCpfTitular() == "030-388-480-00")
		.map(c -> "Conta encontrada: " +
				  c.getNome() + " " +
		          c.getBanco() +  " " +
				  c.getAgencia()+  " " +
				  c.getNumConta())
		.forEach(System.out::println);
		return null;


	}
	
	public static Double consultaSaldo(List<Conta> contas) throws SaldoNegativadoException{
		contas.stream()
		.filter(c -> c.getCpfTitular() == "030-388-480-00")
		.map(c -> "Saldo da conta: " +
		          c.getSaldo())
		.forEach(System.out::println);
		return 0.0;
	}
	
	public static Double saqueValor(String banco, Integer agencia, Integer numConta, Double valorSaque) throws SaldoInsuficienteException {
		Conta conta = contas.stream()
		.filter(c -> c.getBanco() == "Nubank")
		.filter(c -> c.getAgencia() == 0001)
		.filter(c -> c.getNumConta() == 123456)
		.findAny().get();
		if(conta.getSaldo() != null && conta.getSaldo() >= valorSaque) {
			conta.setSaldo(conta.getSaldo() - valorSaque);
			return conta.getSaldo();
		} if(conta.getSaldo() != null && conta.getSaldo() < valorSaque) {
			throw new SaldoInsuficienteException("Saldo Insuficiente para saque!");
		}
		return null;
		
	}
	
}
